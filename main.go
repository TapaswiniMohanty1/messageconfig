package main

import (
	"database/sql"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

var db *sql.DB
var err error

func main() {

	db, err = sql.Open("mysql", "root:@tcp(localhost:3306)/demo?charset=utf8")
	//db, err = sql.Open("mysql", "ecomm_user:Cuk03s2HM93042d@tcp(10.30.1.21:3306)/demo?charset=utf8")

	check(err)

	defer db.Close()

	err = db.Ping()
	check(err)

	InitStore(&dbStore{db: db})

	r := newRouter()

	err := http.ListenAndServe(":8080", r)

	check(err)
}

func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/hello", handler).Methods("GET")

	staticFileDirectory := http.Dir("./assets/")
	templateFileDirectory := http.Dir("./templates/")
	messageListDirectory := http.Dir("./messageList/")
	exportDirectory := http.Dir("./export/")

	staticFileHandler := http.StripPrefix("/home", http.FileServer(staticFileDirectory))
	messageListFileHandler := http.StripPrefix("/messageList", http.FileServer(messageListDirectory))
	exportHandler := http.StripPrefix("/export", http.FileServer(exportDirectory))
	templateFileHandler := http.StripPrefix("/template", http.FileServer(templateFileDirectory))
	previewFileHandler := http.StripPrefix("/preview", http.FileServer(staticFileDirectory))

	r.PathPrefix("/home").Handler(staticFileHandler).Methods("GET")
	r.PathPrefix("/messageList").Handler(messageListFileHandler).Methods("GET")
	r.PathPrefix("/export").Handler(exportHandler).Methods("GET")

	r.HandleFunc("/messages", getMessageHandler).Methods("GET")
	r.HandleFunc("/message", createMessageHandler).Methods("POST")
	r.HandleFunc("/addmessage", addMessageHandler).Methods("POST")
	r.HandleFunc("/messageDetails", addMessageDetailHandler).Methods("POST")

	r.HandleFunc("/updatemessage", updateMessageHandler).Methods("POST")
	r.HandleFunc("/updateTemplatemessage", updateTemplateMessageHandler).Methods("POST")

	r.HandleFunc("/upload", parseCsvHandler).Methods("POST")
	r.HandleFunc("/exportMessage", exportCsvHandler).Methods("POST")
	r.HandleFunc("/downloadMessage", downloadKeyHandler).Methods("POST")

	r.HandleFunc("/previewmessage", previewMessageHandler).Methods("POST")
	r.HandleFunc("/previewTemplatemessage", previewTemplateMessageHandler).Methods("POST")
	r.HandleFunc("/templateMessage", getTemplateMessageHandler).Methods("GET")
	r.HandleFunc("/templateMessage", createTemplateMessageHandler).Methods("POST")
	r.HandleFunc("/templateMessageDetails", createTemplateDetailMessageHandler).Methods("POST")

	r.HandleFunc("/appNames", getAppNameHandler).Methods("GET")
	r.PathPrefix("/template").Handler(templateFileHandler).Methods("GET")
	// r.HandleFunc("/translate", getTranslatedMsgHandler).Methods("POST")

	r.PathPrefix("/preview/").Handler(previewFileHandler).Methods("POST")

	return r
}

func check(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}
