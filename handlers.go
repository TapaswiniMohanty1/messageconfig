package main

import (
	"bytes"
	"crypto/md5"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type CsvLine struct {
	AppName     string `json:"appName"`
	Jiranumber  string `json:"jiranumber"`
	KeyMsg      string `json:"keyMsg"`
	Language    string `json:"language"`
	Value       string `json:"value"`
	Description string `json:"description"`
}

type Message struct {
	ID          string `json:"id"`
	Jiranumber  string `json:"jiranumber"`
	KeyMsg      string `json:"keyMsg"`
	Language    string `json:"language"`
	Value       string `json:"value"`
	Description string `json:"description"`
	AppName     string `json:"appName"`
}

type MessageDetails struct {
	MessageDetailsID string `json:"messageDetailsId"`
	MessageID        string `json:"messageId"`
	KeyMsg           string `json:"keyMsg"`
	Jiranumber       string `json:"jiranumber"`
	Language         string `json:"language"`
	Value            string `json:"value"`
	Description      string `json:"description"`
	AppName          string `json:"appName"`
}

type TemplateMessageDetails struct {
	TemplateMessageDetailsID string `json:"templateMessageDetailsID"`
	TemplateMessageID        string `json:"templateMessageID"`
	TemplateName             string `json:"templateName"`
	TemplateCatagory         string `json:"templateCatagory"`
	Jiranumber               string `json:"jiranumber"`
	Value                    string `json:"value"`
	Description              string `json:"description"`
	AppName                  string `json:"appName"`
}

type TranslationMessageDetails struct {
	TemplateName string `json:"templateName"`
	AppName      string `json:"appName"`
}

type PreviewMessageDetails struct {
	Language string `json:"language"`
	Value    string `json:"value"`
	AppName  string `json:"appName"`
	KeyMsg   string `json:"keyMsg"`
}

type PreviewTemplateMessageDetails struct {
	Language string `json:"language"`
	Value    string `json:"value"`
	AppName  string `json:"appName"`
	KeyMsg   string `json:"keyMsg"`
}

type ExportMessageDetails struct {
	KeyMsg   string `json:"keyMsg"`
	Language string `json:"language"`
	Value    string `json:"value"`
}

type AppNames struct {
	AppName string `json:"appName"`
}

func getAppNameHandler(w http.ResponseWriter, r *http.Request) {

	appNames, err := store.GetAppNames()

	// Everything else is the same as before
	appNamesBytes, err := json.Marshal(appNames)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(appNamesBytes)

}

func getMessageHandler(w http.ResponseWriter, r *http.Request) {

	messages, err := store.GetMessages()

	// Everything else is the same as before
	messageListBytes, err := json.Marshal(messages)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(messageListBytes)

}

func getTemplateMessageHandler(w http.ResponseWriter, r *http.Request) {

	messages, err := store.GetTemplateMessages()

	// Everything else is the same as before
	messageListBytes, err := json.Marshal(messages)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(messageListBytes)

}

func uploadFile(w http.ResponseWriter, r *http.Request) {

	fmt.Println("method:", r.Method)
	if r.Method == "GET" {
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))

		t, _ := template.ParseFiles("upload.gtpl")
		t.Execute(w, token)
	} else {
		r.ParseMultipartForm(32 << 20)
		file, handler, err := r.FormFile("uploadfile")

		if isError(err) {
			return
		}
		defer file.Close()
		fmt.Fprintf(w, "%v", handler.Header)
		f, err := os.Create(handler.Filename)
		application := r.FormValue("table-appname-dropdown")
		output, err := os.Create("output.txt")

		if isError(err) {
			return
		}

		defer f.Close()
		io.Copy(f, file)

		f, err = os.Open(handler.Filename)
		if isError(err) {
			return
		}

		defer f.Close() // this needs to be after the err check

		lines, err := csv.NewReader(f).ReadAll()
		if isError(err) {
			return
		}

		for i, line := range lines {
			if i == 0 {
				// skip header line
				continue
			}
			data := Message{
				AppName:     application,
				Jiranumber:  line[0],
				KeyMsg:      line[1],
				Language:    line[2],
				Value:       line[3],
				Description: line[4],
			}

			id, err := store.CreateMessage(&data)
			if isError(err) {
				fmt.Println(id, output)

				f.Sync()
			}

			err = store.CreateMessageDetailsForExistingKey(id, &data)
			if isError(err) {
				fmt.Println(id, output)

			}
		}

		defer f.Close()
		err = os.Remove(handler.Filename)

	}
	if isError(err) {
		fmt.Println(err)

	}

}

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}
func parseCsvHandler(w http.ResponseWriter, r *http.Request) {
	uploadFile(w, r)
}
func notificationTemplateHandler(w http.ResponseWriter, r *http.Request) {
	uploadFile(w, r)
}

func downloadKeyHandler(w http.ResponseWriter, r *http.Request) {
	message := Message{}
	_ = json.NewDecoder(r.Body).Decode(&message)

	messageList, err := store.DownlaodExportMessages(&message)
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fileName := message.AppName + ".txt"
	f, err := os.Create(strings.Join([]string{dir + "/export/", fileName}, "/"))

	if err != nil {
		fmt.Println(err)
		return
	}
	f.WriteString("\"" + "keyMsg" + "\",\"" + "language" + "\"" + "\n")
	for _, elem := range messageList {
		if _, err = f.WriteString("\"" + elem.KeyMsg + "\",\"" + elem.Language + "\"" + "\n"); err != nil {

		}

	}
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	if err != nil {
		log.Fatal("Cannot encode to JSON ", err)
	}
	pwd, _ := os.Getwd()
	downloadBytes, err := ioutil.ReadFile(pwd + "/export/" + fileName)

	if err != nil {
		fmt.Println(err)
	}

	// set the default MIME type to send
	mime := http.DetectContentType(downloadBytes)

	fileSize := len(string(downloadBytes))

	// Generate the server headers
	w.Header().Set("Content-Type", mime)
	w.Header().Set("Content-Disposition", "attachment; filename=CMS.txt")
	w.Header().Set("Expires", "0")
	w.Header().Set("Content-Transfer-Encoding", "binary")
	w.Header().Set("Content-Length", strconv.Itoa(fileSize))
	w.Header().Set("Content-Control", "private, no-transform, no-store, must-revalidate")

	http.ServeContent(w, r, fileName, time.Now(), bytes.NewReader(downloadBytes))
	io.Copy(w, bytes.NewReader(downloadBytes))

}

func exportCsvHandler(w http.ResponseWriter, r *http.Request) {
	message := Message{}
	_ = json.NewDecoder(r.Body).Decode(&message)
	messageList, err := store.GetExportMessages(&message)
	if err != nil {
		fmt.Println(err)
		return
	}
	if err != nil {
		fmt.Println(err)
		return
	}
	messageJSON, err := json.Marshal(messageList)
	if err != nil {
		log.Fatal("Cannot encode to JSON ", err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(messageJSON)
}

func addMessageHandler(w http.ResponseWriter, r *http.Request) {
	var messages []Message
	var msg Message
	_ = json.NewDecoder(r.Body).Decode(&msg)
	messages = append(messages, msg)
	json.NewEncoder(w).Encode(messages)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var tempMsg *Message = &msg

	id, err := store.CreateMessage(tempMsg)
	if isError(err) {
		fmt.Println(id, err)

		return
	}

}
func addMessageDetailHandler(w http.ResponseWriter, r *http.Request) {
	var messages []Message

	var msg Message
	_ = json.NewDecoder(r.Body).Decode(&msg)
	messages = append(messages, msg)
	json.NewEncoder(w).Encode(messages)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var tempMsg *Message = &msg

	err = store.CreateMessageDetails(tempMsg)
	if isError(err) {
		return
	}

}

func createTemplateDetailMessageHandler(w http.ResponseWriter, r *http.Request) {
	var messages []TemplateMessageDetails

	var msg TemplateMessageDetails
	_ = json.NewDecoder(r.Body).Decode(&msg)
	messages = append(messages, msg)
	json.NewEncoder(w).Encode(messages)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var tempMsg *TemplateMessageDetails = &msg

	id, err := store.CreateTemplateMessageDetails(tempMsg)
	if isError(err) {
		return
	}
	log.Println("template details ", id)

}

func previewTemplateMessageHandler(w http.ResponseWriter, r *http.Request) {
	var previewTemplateMsgDetail PreviewTemplateMessageDetails
	_ = json.NewDecoder(r.Body).Decode(&previewTemplateMsgDetail)
	var tempMsg *PreviewTemplateMessageDetails = &previewTemplateMsgDetail

	parsedTemplateMsg, err := store.ParseTemplateMessageDetails(tempMsg)

	if isError(err) {
		json.NewEncoder(w).Encode(err)
	}
	if err != nil {
		log.Fatal("Cannot encode to JSON ", err)
	}
	// w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(parsedTemplateMsg)

}

func previewMessageHandler(w http.ResponseWriter, r *http.Request) {
	var previewMsgDetail PreviewMessageDetails
	_ = json.NewDecoder(r.Body).Decode(&previewMsgDetail)
	var tempMsg *PreviewMessageDetails = &previewMsgDetail

	parsedMsg, err := store.ParseMessageDetails(tempMsg)

	if isError(err) {
		json.NewEncoder(w).Encode(err)
	}
	if err != nil {
		log.Fatal("Cannot encode to JSON ", err)
	}
	// w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(parsedMsg)

}
func updateMessageHandler(w http.ResponseWriter, r *http.Request) {
	var messageDetailList []MessageDetails

	var msgDetail MessageDetails
	_ = json.NewDecoder(r.Body).Decode(&msgDetail)
	messageDetailList = append(messageDetailList, msgDetail)
	json.NewEncoder(w).Encode(messageDetailList)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var tempMsg *MessageDetails
	tempMsg = &msgDetail

	err = store.UpdateMessage(tempMsg)
	if err != nil {
		fmt.Println(err)
	}

}

func updateTemplateMessageHandler(w http.ResponseWriter, r *http.Request) {
	var messageDetailList []TemplateMessageDetails

	var msgDetail TemplateMessageDetails
	_ = json.NewDecoder(r.Body).Decode(&msgDetail)
	messageDetailList = append(messageDetailList, msgDetail)
	json.NewEncoder(w).Encode(messageDetailList)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var tempMsg *TemplateMessageDetails
	tempMsg = &msgDetail

	err = store.UpdateTemplateMessage(tempMsg)
	if err != nil {
		fmt.Println(err)
	}

}

func createMessageHandler(w http.ResponseWriter, r *http.Request) {
	message := Message{}

	err := r.ParseForm()

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	message.Jiranumber = r.Form.Get("jiranumber")
	message.Language = r.Form.Get("language")

	id, err := store.CreateMessage(&message)
	if err != nil {
		fmt.Println(id, err)
	}

}

func createTemplateMessageHandler(w http.ResponseWriter, r *http.Request) {

	var messages []TemplateMessageDetails

	var msg TemplateMessageDetails
	_ = json.NewDecoder(r.Body).Decode(&msg)
	messages = append(messages, msg)
	json.NewEncoder(w).Encode(messages)

	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var tempMsg *TemplateMessageDetails = &msg

	id, err := store.CreateTemplatesMessage(tempMsg)
	if isError(err) {
		fmt.Println(id, err)

		return
	}

}
