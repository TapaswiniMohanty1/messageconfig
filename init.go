package main

import (
	"bitbucket.org/libertywireless/messageConfig/config"
)

// App struct
type App struct {
	config *config.GeneralConfig
}

// NewApp create app with config
func NewApp(configPath string) *App {
	generalConfig := config.LoadConfig(configPath)
	app := &App{
		config: generalConfig,
	}
	return app
}
