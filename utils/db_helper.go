package utils

import "github.com/jmoiron/sqlx"

// RowsToMap - Converts sql Rows to a map
func RowsToMap(rows *sqlx.Rows) *[]map[string]interface{} {
	var result []map[string]interface{}
	for rows.Next() {
		res := make(map[string]interface{})
		rows.MapScan(res)
		result = append(result, res)
	}
	return &result

}
