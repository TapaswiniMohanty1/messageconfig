package utils

import (
	"github.com/jmoiron/sqlx"
)

type methodType func(db *sqlx.DB, data *map[string]interface{}) interface{}

// SqlxExecute execute the method and close connection
// method definition => func(db *sqlx.DB, data *map[string]interface{}) interface{}
func SqlxExecute(method methodType, options *map[string]interface{}) (data interface{}) {
	conn, _ := sqlx.Connect("mysql", "root@tcp(localhost:3306)/demo?parseTime=true")
	data = method(conn, options)
	defer conn.Close()
	return
}
