package utils

import (
	"fmt"
	"strings"
)

// SliceContainsString returns true if a slice contains a string.
func SliceContainsString(sl *[]string, str string) bool {
	for _, n := range *sl {
		if str == n {
			return true
		}
	}
	return false
}

// JoinInterface joins interface and returns array
func JoinInterface(d *[]interface{}, del string) string {
	return strings.Trim(strings.Join(strings.Fields(fmt.Sprint(d)), del), "[]")
}

// StringUniq returns uniq strings from slice
func StringUniq(elements *[]string) *[]string {
	encountered := map[string]bool{}
	result := []string{}
	for v := range *elements {
		if encountered[(*elements)[v]] != true {
			encountered[(*elements)[v]] = true
			result = append(result, (*elements)[v])
		}
	}
	return &result
}
