package utils

import (
	"strings"
)

type ciMap struct {
	m map[string]interface{}
}

// NewCiMap is the new case insensitive
func NewCiMap() ciMap {
	return ciMap{m: make(map[string]interface{})}
}

func (m ciMap) set(s string, b interface{}) {
	m.m[strings.ToLower(s)] = b
}

func (m ciMap) get(s string) (b interface{}, ok bool) {
	b, ok = m.m[strings.ToLower(s)]
	return
}
