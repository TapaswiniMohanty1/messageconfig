package utils

import (
	"reflect"
)

// GetTagValue return a particular tag value
func GetTagValue(t reflect.Type, field string, tagName string) string {
	var tagValue string
	fn, _ := t.FieldByName(field)
	tagValue, _ = fn.Tag.Lookup(tagName)
	return tagValue
}
