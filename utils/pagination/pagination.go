package pagination

import "bitbucket.org/libertywireless/messageConfig/constant"

// Decode returns limit and offset
// 1st param is limit
// 2nd param is page_number to calculate offset
func Decode(paginationParams ...int) (offset int, limit int) {
	paramCount := len(paginationParams)
	pgNo := 1
	limit = constant.DBResultLimit
	if paramCount > 0 && paginationParams[0] != 0 {
		limit = paginationParams[0]
	}
	if paramCount > 1 && paginationParams[1] != 0 {
		pgNo = paginationParams[1]
	}
	offset = (pgNo - 1) * limit
	return
}
