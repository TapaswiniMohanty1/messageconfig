package utils

import "fmt"

// GroupBy - Groups the result on the basis of a key
func GroupBy(records *[]map[string]interface{}, k string) *map[string][]map[string]interface{} {
	res := make(map[string][]map[string]interface{})
	for _, record := range *records {
		if v, ok := record[k]; ok {
			key := fmt.Sprintf("%v", v)
			res[key] = append(res[key], record)
		}
	}
	return &res
}
