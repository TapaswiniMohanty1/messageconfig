package utils

import (
	uuid "github.com/satori/go.uuid"
)

// UuidToString converts a uuid to string
func UuidToString(binaryValue []byte) (val string, err error) {
	uuidValue, err := uuid.FromBytes(binaryValue)
	if err != nil {
		return
	}
	val = uuidValue.String()
	return
}

// StringToBinary converts a string to uuid
func StringToBinary(stringValue string) (binaryValue []byte, err error) {
	uuidValue, err := uuid.FromString(stringValue)
	if err != nil {
		return
	}
	return uuidValue.Bytes(), nil
}
