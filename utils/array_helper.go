package utils

import (
	"math/rand"
	"time"
)

//RandomSample randomly select `l` elements from the given inp array
func RandomSample(inp *[]interface{}, l int) *[]interface{} {
	res := make([]interface{}, l)
	r := rand.New(rand.NewSource(time.Now().Unix()))
	perm := r.Perm(len(*inp))
	for i := 0; i < l; i++ {
		res[i] = (*inp)[perm[i]]
	}
	return &res
}
