package utils

// Min minimum amongst ints
func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

// Max maximum amongst ints
func Max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
