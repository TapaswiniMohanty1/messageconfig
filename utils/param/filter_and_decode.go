package param

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"bitbucket.org/libertywireless/messageConfig/utils"
)

// FilterAndDecode filter out all the permitted keys from body
func FilterAndDecode(body io.ReadCloser, keys *[]string, v interface{}) error {
	filteredParamByteArr, err := permit(body, keys)
	if err != nil {
		return err
	}
	json.Unmarshal(filteredParamByteArr, v)
	return nil
}

func permit(body io.ReadCloser, keys *[]string) ([]byte, error) {
	paramsByteArr, err := ioutil.ReadAll(body)
	if err != nil {
		return nil, err
	}
	keysMap := arrayToMap(keys)
	params := make(map[string]interface{})
	json.Unmarshal(paramsByteArr, &params)
	// Filtering keys, and converting all `_id` to []byte
	for key := range params {
		if _, ok := keysMap[key]; !ok {
			delete(params, key)
			continue
		}
		if key[(len(key)-2):] == "id" {
			params[key], _ = utils.StringToBinary(params[key].(string))
		}
	}
	return json.Marshal(params)
}

func arrayToMap(keys *[]string) map[string]struct{} {
	keysMap := make(map[string]struct{})
	for _, key := range *keys {
		keysMap[key] = struct{}{}
	}
	return keysMap
}
