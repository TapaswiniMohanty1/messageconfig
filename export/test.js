var arrHead = new Array();
arrHead = [  'Language',"Value",'Description','Preview','Action'];

lang = [  'EN', 'CH','IN'];


 

function preview(oButton) {
    var empTab = document.getElementById('newMessageTable');
    var appName = $( "#appname-dropdown option:selected" ).text();

    var value = oButton.parentNode.parentNode.cells[1].childNodes[0].value;

    var language = oButton.parentNode.parentNode.cells[0].innerText
    previewMsg={
        "appName" : appName,
        "value" : value,
        "language"   : language
    };
    var previewMsgObjStr = JSON.stringify(previewMsg);
    jQuery.ajax({
        type: 'POST',
        url: '/previewmessage',
        data: previewMsgObjStr,
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            console.log('success');
            alert(data);

            var h = 650;
            var w = 340;
            var wh = screen.height;
            var ww = screen.width;
            var top = wh/2 - h/2;
            var left = ww/2 - w/2;
            var popup = window.open( '', 'player', 'height=' + wh + ', width=' + w + ', scrollbars=no, left=' + left + ', top=' + top);
            var $popup = $(popup.document.body);
            $popup.html('<textarea style="height:50%;width:100% ">'+data+'</textarea>');

            // var popup = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto, scrollbars=yes');

        },
        error: function (xhr, textStatus, errorThrown) {
            console.log('Error in Operation');
        }
    });
}
function editRow(){
    $('.editbtn').click(function () {
        var currentTD = $(this).closest('tr');

        if ($(this).html() == 'Edit') {
           $(currentTD).find('.inputDisabled').prop("disabled",false);
        } else {
           $(currentTD).find('.inputDisabled').prop("disabled",true);

        }
    });


}

function exportMessages() {
    var myTab = document.getElementById('exportTab');
    var appName = $( "#exportTab-appname-dropdown option:selected" ).text();
    if (appName == "")
    {
    alert("Please select an Application name");
    return false;
    }
    var countries = [];
    $.each($("#language_dropdown option:selected"), function(){
        countries.push($(this).val());
    });
    var exportFilter = [];
    // LOOP THROUGH EACH ROW OF THE TABLE.
        exportFilter.push({
            "appName" : appName,
            "language" : countries
        });
        var obj = exportFilter[0];
        var myObjStr = JSON.stringify(obj);
        jQuery.ajax({
            type: 'POST',
            url: '/exportMessage',
            data: myObjStr,
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                console.log('exported successfully');
                 },
            error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Operation'+errorThrown+textStatus+xhr);
            }
        });
}

function downloadMessages() {
    var myTab = document.getElementById('exportTab');
    var appName = $( "#exportTab-appname-dropdown option:selected" ).text();
    if (appName == "")
    {
    alert("Please select an Application name");
    return false;
    }
    var countries = [];
    $.each($("#language_dropdown option:selected"), function(){
        countries.push($(this).val());
    });
    var exportFilter = [];
    // LOOP THROUGH EACH ROW OF THE TABLE.
        exportFilter.push({
            "appName" : appName,
            "language" : countries
        });
        var obj = exportFilter[0];
        var myObjStr = JSON.stringify(obj);
        jQuery.ajax({
            type: 'POST',
            url: '/downloadMessage',
            data: myObjStr,
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                console.log('exported successfully');
                 },
            error: function (xhr, textStatus, errorThrown) {
                console.log('Error in Operation');
            }
        });
}


$(document).ready(function () {
	let dropdown = $('#appname-dropdown');
	let dropdowntable = $('#table-appname-dropdown');
	let exportTab_dropdown  = $('#exportTab-appname-dropdown');
    dropdowntable.empty();
    dropdowntable.prop('selectedIndex', 0);
	const url = '/appNames';
    dropdown.empty();
	dropdown.prop('selectedIndex', 0);
    // Populate dropdown with list of appNames
    $.getJSON(url, function (data) {
		$.each(data, function (key, entry) {
		dropdown.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
		dropdowntable.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
		exportTab_dropdown.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
		})
	});
});

			