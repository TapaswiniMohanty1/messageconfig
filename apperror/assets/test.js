
   function htmlbodyHeightUpdate(){
    var height3 = $( window ).height()
    var height1 = $('.nav').height()+50
    height2 = $('.main').height()
    if(height2 > height3){
        $('html').height(Math.max(height1,height3,height2)+10);
        $('body').height(Math.max(height1,height3,height2)+10);
    }
    else
    {
        $('html').height(Math.max(height1,height3,height2));
        $('body').height(Math.max(height1,height3,height2));
    }
    
}
$(document).ready(function () {
    htmlbodyHeightUpdate()
    $( window ).resize(function() {
        htmlbodyHeightUpdate()
    });
    $( window ).scroll(function() {
        height2 = $('.main').height()
          htmlbodyHeightUpdate()
    });
});
var arrHead = new Array();
arrHead = [  'Language',"Value",'Description','Action']; 

lang = [  'EN', 'CH','IN']; 


 function removeRow(oButton) {
    var empTab = document.getElementById('newMessageTable');
    empTab.deleteRow(oButton.parentNode.parentNode.rowIndex); 
}

function preview(oButton) {
    var empTab = document.getElementById('newMessageTable');
    var appName = $( "#appname-dropdown option:selected" ).text();

    var value = oButton.parentNode.parentNode.cells[1].childNodes[0].value;

    var language = oButton.parentNode.parentNode.cells[0].innerText
    previewMsg={ 
        "appName" : appName,
        "value" : value,
        "language"   : language 
    };
    var previewMsgObjStr = JSON.stringify(previewMsg);
    jQuery.ajax({
        type: 'POST',
        url: '/previewmessage',
        data: previewMsgObjStr,
        dataType: 'json',
        success: function (data, textStatus, xhr) {  
            console.log('success');  
            alert(data);

            var h = 650;
            var w = 340;
            var wh = screen.height;
            var ww = screen.width;
            var top = wh/2 - h/2;
            var left = ww/2 - w/2;
            var popup = window.open( '', 'player', 'height=' + wh + ', width=' + w + ', scrollbars=no, left=' + left + ', top=' + top);
            var $popup = $(popup.document.body);
            $popup.html('<textarea style="height:50%;width:100% ">'+data+'</textarea>');
            
            // var popup = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto, scrollbars=yes');
               
        },  
        error: function (xhr, textStatus, errorThrown) {  
            console.log('Error in Operation');  
        }  
    });
}

function editRow(){   
    $('.editbtn').click(function () {
        var currentTD = $(this).closest('tr');

        if ($(this).html() == 'Edit') {                  
           $(currentTD).find('.inputDisabled').prop("disabled",false);   
        } else {
           $(currentTD).find('.inputDisabled').prop("disabled",true);   
  
        }
    }); 

  
}
function addRow() {
    
    var appName = $( "#appname-dropdown option:selected" ).text();
    if (appName == "")
    {
    alert("Please select an Application name");
    return false;
    }
    var jiranumber = document.getElementById('jiranumbernew').value;
    
    var keyname = document.getElementById('keynew').value;
    if (jiranumber == "")
    {
    alert("Please input a jiranumber");
    return false;
    }
    if (keyname == "")
    {
    alert("Please input a key");
    return false;
    }
    

    var empTab = document.getElementById('newMessageTable');

    for( ctr =0; ctr < lang.length; ctr++){


    var rowCnt = empTab.rows.length;        // GET TABLE ROW COUNT.
    var tr = empTab.insertRow(rowCnt);      // TABLE ROW.
    for (var c = 0; c < arrHead.length; c++) {
        var td = document.createElement('td'); 
 
        td = tr.insertCell(c);

        if ( c == 1 || c == 2) {    
            var input = document.createElement('TEXTAREA');
            input.setAttribute('name', 'post');
            //input.setAttribute('maxlength', 5000);
            input.setAttribute('cols',25);
            input.setAttribute('rows', 2);
            input.setAttribute('value','');
            td.appendChild(input);
            td.width = '25%';
              
        } else if (c == 3) {  
            var button = document.createElement('input');
            button.setAttribute('type', 'button');
            button.setAttribute('value', 'Remove');
            button.setAttribute('onclick', 'removeRow(this)');
            td.appendChild(button);

        }
       

        else if (c == 0){
        td.innerText=lang[ctr];
        td.width = '20%';


        }
    }
  }
}

function exportMessages() {
    var myTab = document.getElementById('exportTab');
    var appName = $( "#exportTab-appname-dropdown option:selected" ).text();
    if (appName == "")
    {
    alert("Please select an Application name");
    return false;
    }
    var countries = [];
    $.each($("#language_dropdown option:selected"), function(){            
        countries.push($(this).val());
    });


    var exportFilter = [];    

    // LOOP THROUGH EACH ROW OF THE TABLE.
        exportFilter.push({ 
            "appName" : appName,
            "language" : countries

           
        });
        var obj = exportFilter[0];
        var myObjStr = JSON.stringify(obj);
        jQuery.ajax({
            type: 'POST',
            url: '/exportMessage',
            data: myObjStr,
            dataType: 'json',
            success: function (data, textStatus, xhr) {  
                console.log('exported successfully');  
                 },  
            error: function (xhr, textStatus, errorThrown) {  
                console.log('Error in Operation');  
            }  
        });


}


function submit() {
    var myTab = document.getElementById('newMessageTable');
    var jiranumber = document.getElementById('jiranumbernew').value;
    var keyname = document.getElementById('keynew').value;
    var appName = $( "#appname-dropdown option:selected" ).text();

    var messageList = [];    

    // LOOP THROUGH EACH ROW OF THE TABLE.
    for (row = 1; row < myTab.rows.length ; row++) {
        var language = myTab.rows.item(row).cells[0].innerText
        var value = myTab.rows.item(row).cells[1].childNodes[0].value
        var description = myTab.rows.item(row).cells[2].childNodes[0].value
        messageList.push({ 
            "appName" : appName,
            "jiranumber" :  jiranumber,
            "keyMsg" : keyname,
            "value" : value,
            "description"  : description,
            "language"   : language 
        });

}
saveMessages(messageList);
}


function saveMessages(jsonObj){
    var obj = jsonObj[0];
    var myObjStr = JSON.stringify(obj);
    jQuery.ajax({
        type: 'POST',
        url: '/addmessage',
        data: myObjStr,
        dataType: 'json',
        success: function (data, textStatus, xhr) {  
            addMessageDetails(jsonObj);        },  
        error: function (xhr, textStatus, errorThrown) {  
            console.log('Error in Operation');  
        }  
    });


}

function addMessageDetails(jsonObj){
    for(var i = 0; i < jsonObj.length; i++) {
        var obj = jsonObj[i];
        var myObjStr = JSON.stringify(obj);
        jQuery.ajax({
          type: 'POST',
          url: '/messageDetails',
          data: myObjStr,
          dataType: 'json',
          success: function (data, textStatus, xhr) {  
          },  
          error: function (xhr, textStatus, errorThrown) {  
              console.log('Error in Operation');  
          }  
      });
        
      }
      document.location.reload() ;
}




$(document).ready(function () {

    $("#upload").click(function (event) {
        var appName = $( "#table-appname-dropdown option:selected" ).text();
        if (appName == "")
        {
        alert("Please select an Application name");
        return false;
        }


        //stop submit the form, we will post it manually.
        event.preventDefault();

        // var form = $('#uploadform')[0];
        // var data = new FormData(form);

        var formData =$("#uploadform").submit(function(e){
            return ;
        });
        var formData = new FormData(formData[0]); 

		
		// disabled the submit button
        $("#upload").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/upload",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#upload").prop("disabled", false);
                document.location.reload() ;

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#upload").prop("disabled", false);

            }
        });

    });

    let dropdown = $('#appname-dropdown');
    let dropdowntable = $('#table-appname-dropdown');
    let exportTab_dropdown  = $('#exportTab-appname-dropdown');


    dropdowntable.empty(); 
    dropdowntable.prop('selectedIndex', 0);
    const url = '/appNames';
 
    dropdown.empty(); 
    dropdown.prop('selectedIndex', 0);
    // Populate dropdown with list of appNames
    $.getJSON(url, function (data) {
      $.each(data, function (key, entry) {
        dropdown.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
        dropdowntable.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
        exportTab_dropdown.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
      })
    });


});

