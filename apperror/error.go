// error usage
// err := apperror.Fetch("test") -> returns an error struct
// fmt.Println(err) for logging

package apperror

import (
	"fmt"
	"time"

	"bitbucket.org/libertywireless/messageConfig/logging"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

var errors = make(map[string]AppError)

const filepath = "error.yml"

// AppError Custom Application Error
type AppError struct {
	Code    int16     `json:"code" mapstruct:"code"`
	Time    time.Time `json:"time"`
	Message string    `json:"message" mapstruct:"message"`
	Err     []string  `json:"err" mapstruct:"err"`
}

// NewAppError returns error with default time
func NewAppError() *AppError {
	err := AppError{Time: time.Now()}
	return &err
}

// fmt.Println(err) for logging an AppError
func (e AppError) Error() string {
	return fmt.Sprintf("%v, ERROR (%v, %v), %v", e.Time, e.Code, e.Message, e.Err)
}

// InitErrors loads all the errors
func InitErrors(locale string) {
	viper.SetConfigFile(filepath)
	err := viper.ReadInConfig()
	var errs map[string]interface{}
	mapstructure.Decode(viper.GetStringMap("errors")[locale], &errs)
	for k, v := range errs {
		appErr := NewAppError()
		mapstructure.Decode(v, &appErr)
		errors[k] = *appErr
	}
}

// Fetch returns specific app error
func Fetch(key string, errs ...error) *AppError {
	apperr := errors[key]
	for _, err := range errs {
		apperr.Err = append(apperr.Err, err.Error())
	}
	return &apperr
}

// DefaultError returns application
func DefaultError(err ...error) *AppError {
	logging.LogErrorFields(map[string]interface{}{
		"message": "Default Error",
		"record":  err})
	apperr := Fetch("default", err...)
	return apperr
}
