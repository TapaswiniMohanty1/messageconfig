package main

import (
	"database/sql"
	"regexp"
	"strings"
)

type Store interface {
	GetMessages() ([]*MessageDetails, error)
	GetAppNames() ([]*AppNames, error)
	GetTemplateMessages() ([]*TemplateMessageDetails, error)
	CreateMessage(message *Message) (int, error)
	CreateTemplatesMessage(message *TemplateMessageDetails) (int, error)
	CreateTemplateMessageDetails(message *TemplateMessageDetails) (int, error)

	CreateMessageDetailsForExistingKey(id int, message *Message) error
	UpdateMessage(message *MessageDetails) error
	UpdateTemplateMessage(message *TemplateMessageDetails) error

	CreateMessageDetails(message *Message) error
	GetExportMessages(message *Message) ([]ExportMessageDetails, error)
	DownlaodExportMessages(message *Message) ([]ExportMessageDetails, error)
	ParseMessageDetails(previewMsgDetail *PreviewMessageDetails) (string, error)
	ParseTemplateMessageDetails(previewTemplateMsgDetail *PreviewTemplateMessageDetails) (string, error)
}

// The `dbStore` struct will implement the `Store` interface
// It also takes the sql DB connection object, which represents
// the database connection.
type dbStore struct {
	db *sql.DB
}

func (store *dbStore) CreateMessage(message *Message) (int, error) {
	var id int
	err := db.QueryRow("SELECT id FROM messages where keymsg = ?  and appName = ? ", message.KeyMsg, message.AppName).Scan(&id)

	if id == 0 {
		err = nil
		rows, err := store.db.Query("INSERT INTO messages(Jiranumber, keymsg, appName) VALUES (?,?,?)", message.Jiranumber, message.KeyMsg, message.AppName)
		if err != nil {
			return id, err
		}
		defer rows.Close()

		err = db.QueryRow("SELECT id FROM messages where keymsg = ? and Jiranumber = ? and appName = ? ", message.KeyMsg, message.Jiranumber, message.AppName).Scan(&id)
		if err != nil {
			return id, err
		}
		defer rows.Close()
	}

	return id, err

}

func (store *dbStore) CreateMessageDetailsForExistingKey(messageId int, message *Message) error {
	var id int
	if id != 0 {
		err := db.QueryRow("SELECT id FROM messageDetails where messageId = ? and language = ? ", messageId, message.Language).Scan(&id)
		if err != nil {
			return err
		}
	}

	if id == 0 {
		rows, err := store.db.Query("INSERT INTO messageDetails(messageId,value, description,language) VALUES (?,?,?,?)", messageId, strings.TrimSpace(message.Value), message.Description, message.Language)
		defer rows.Close()
		if err != nil {
			return err
		}
	}
	return err
}

func (store *dbStore) CreateTemplatesMessage(message *TemplateMessageDetails) (int, error) {
	var id int
	err := db.QueryRow("SELECT id FROM template where templateName = ?  and appName = ? ", message.TemplateName, message.AppName).Scan(&id)

	if id == 0 {
		err = nil
		rows, err := store.db.Query("INSERT INTO template(Jiranumber, templateName, appName,description) VALUES (?,?,?,?)", message.Jiranumber, message.TemplateName, message.AppName, message.Description)
		if err != nil {
			return id, err
		}
		defer rows.Close()

		err = db.QueryRow("SELECT id FROM template where templateName = ? and Jiranumber = ? and appName = ? ", message.TemplateName, message.Jiranumber, message.AppName).Scan(&id)
		if err != nil {
			return id, err
		}
		defer rows.Close()
	}

	return id, err

}

func (store *dbStore) CreateTemplateMessageDetails(message *TemplateMessageDetails) (int, error) {
	var id int
	err := db.QueryRow("SELECT id FROM template where templateName = ? and Jiranumber = ? and appName = ?  ", message.TemplateName, message.Jiranumber, message.AppName).Scan(&id)
	if err != nil {
		return 0, err
	}
	rows, err := store.db.Query("INSERT INTO templateDetail(templateId,value, templateCatagory) VALUES (?,?,?)", id, strings.TrimSpace(message.Value), message.TemplateCatagory)
	defer rows.Close()
	if err != nil {
		return 0, err
	}
	return id, err
}

func (store *dbStore) CreateMessageDetails(message *Message) error {
	var id int
	err := db.QueryRow("SELECT id FROM messages where keymsg = ? and Jiranumber = ? ", message.KeyMsg, message.Jiranumber).Scan(&id)
	if err != nil {
		return err
	}
	rows, err := store.db.Query("INSERT INTO messageDetails(messageId,value, description,language) VALUES (?,?,?,?)", id, strings.TrimSpace(message.Value), message.Description, message.Language)
	defer rows.Close()
	if err != nil {
		return err
	}
	return err
}

func (store *dbStore) UpdateMessage(messageDetails *MessageDetails) error {
	rows, err := store.db.Query("Update  messageDetails SET  description = ? , value = ? where ID = ? ", messageDetails.Description, strings.TrimSpace(messageDetails.Value), messageDetails.MessageDetailsID)
	defer rows.Close()
	return err
}

func (store *dbStore) UpdateTemplateMessage(messageDetails *TemplateMessageDetails) error {
	rows, err := store.db.Query("Update  templateDetail SET   value = ? where ID = ? ", strings.TrimSpace(messageDetails.Value), messageDetails.TemplateMessageDetailsID)
	defer rows.Close()

	return err
}

func (store *dbStore) GetMessages() ([]*MessageDetails, error) {
	rows, err := store.db.Query("SELECT appName , md.id  as messageDetailsId,m.id  as messageId,jiranumber,keyMsg, value,description,md.language from messages m INNER JOIN messageDetails  md on md.messageId=m.id order by md.id desc")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	messageDetails := []*MessageDetails{}
	for rows.Next() {
		messageDetail := &MessageDetails{}

		if err := rows.Scan(&messageDetail.AppName, &messageDetail.MessageDetailsID, &messageDetail.MessageID, &messageDetail.Jiranumber, &messageDetail.KeyMsg, &messageDetail.Value, &messageDetail.Description, &messageDetail.Language); err != nil {
			return nil, err
		}

		messageDetails = append(messageDetails, messageDetail)
		// fmt.Println(messageDetails)
	}
	return messageDetails, nil
}

func (store *dbStore) GetTemplateMessages() ([]*TemplateMessageDetails, error) {
	rows, err := store.db.Query("SELECT appName , td.id  as templateMessageDetailsId,t.id  as templateMessageId,jiranumber,templateName, value,description,td.templateCatagory from template t INNER JOIN templateDetail  td on td.templateId=t.id order by t.id desc")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	templateMessageDetails := []*TemplateMessageDetails{}
	for rows.Next() {
		messageDetail := &TemplateMessageDetails{}

		if err := rows.Scan(&messageDetail.AppName, &messageDetail.TemplateMessageDetailsID, &messageDetail.TemplateMessageID, &messageDetail.Jiranumber, &messageDetail.TemplateName, &messageDetail.Value, &messageDetail.Description, &messageDetail.TemplateCatagory); err != nil {
			return nil, err
		}

		templateMessageDetails = append(templateMessageDetails, messageDetail)
		// fmt.Println(messageDetails)
	}
	return templateMessageDetails, nil
}

func (store *dbStore) GetAppNames() ([]*AppNames, error) {
	rows, err := store.db.Query("SELECT applicationName FROM Application order by applicationName")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	appNameList := []*AppNames{}
	for rows.Next() {
		appName := &AppNames{}

		if err := rows.Scan(&appName.AppName); err != nil {
			return nil, err
		}

		appNameList = append(appNameList, appName)
		// fmt.Println(messageDetails)
	}
	return appNameList, nil
}

func (store *dbStore) DownlaodExportMessages(message *Message) ([]ExportMessageDetails, error) {
	// var keyMsg string
	exportmessageDetails := []ExportMessageDetails{}
	languageList := message.Language
	s := strings.Split(languageList, ",")

	if languageList != "" {
		rows, err := db.Query("SELECT KeyMsg,language,value FROM messages m  INNER JOIN messageDetails  md on md.messageId=m.id where appName = ? and language in (?)  ", message.AppName, s)
		defer rows.Close()
		for rows.Next() {
			exportmessageDetail := ExportMessageDetails{}

			if err := rows.Scan(&exportmessageDetail.KeyMsg, &exportmessageDetail.Language, &exportmessageDetail.Value); err != nil {
				return nil, err
			}
			if err != nil {
				return nil, err
			}
			exportmessageDetails = append(exportmessageDetails, exportmessageDetail)

		}
	} else {
		rows, err := db.Query("SELECT KeyMsg,language,value FROM messages m  INNER JOIN messageDetails  md on md.messageId=m.id where appName = ?   ", message.AppName)
		defer rows.Close()
		for rows.Next() {
			exportmessageDetail := ExportMessageDetails{}

			if err := rows.Scan(&exportmessageDetail.KeyMsg, &exportmessageDetail.Language, &exportmessageDetail.Value); err != nil {
				return nil, err
			}

			exportmessageDetails = append(exportmessageDetails, exportmessageDetail)
			if err != nil {
				return nil, err
			}
		}
	}

	if err != nil {
		return nil, err
	}
	return exportmessageDetails, nil
}

func (store *dbStore) GetExportMessages(message *Message) ([]ExportMessageDetails, error) {
	// var keyMsg string
	exportmessageDetails := []ExportMessageDetails{}
	languageList := message.Language
	s := strings.Split(languageList, ",")

	if languageList != "" {
		rows, err := db.Query("SELECT KeyMsg,language,value FROM messages m  INNER JOIN messageDetails  md on md.messageId=m.id where appName = ? and language in (?)  ", message.AppName, s)
		defer rows.Close()
		for rows.Next() {
			exportmessageDetail := ExportMessageDetails{}

			if err := rows.Scan(&exportmessageDetail.KeyMsg, &exportmessageDetail.Language, &exportmessageDetail.Value); err != nil {
				return nil, err
			}
			if err != nil {
				return nil, err
			}
			exportmessageDetails = append(exportmessageDetails, exportmessageDetail)

		}
	} else {
		rows, err := db.Query("SELECT KeyMsg,language,value FROM messages m  INNER JOIN messageDetails  md on md.messageId=m.id where appName = ?   ", message.AppName)
		defer rows.Close()
		for rows.Next() {
			exportmessageDetail := ExportMessageDetails{}

			if err := rows.Scan(&exportmessageDetail.KeyMsg, &exportmessageDetail.Language, &exportmessageDetail.Value); err != nil {
				return nil, err
			}

			exportmessageDetails = append(exportmessageDetails, exportmessageDetail)
			if err != nil {
				return nil, err
			}
		}
	}

	if err != nil {
		return nil, err
	}
	return exportmessageDetails, nil
}

func (store *dbStore) ParseMessageDetails(previewMsgDetail *PreviewMessageDetails) (string, error) {

	var value = strings.TrimSpace(previewMsgDetail.Value)

	rex := regexp.MustCompile(`\{{(.*?)\}}`)
	out := rex.FindAllStringSubmatch(value, -1)

	query := "SELECT list.keyMsg ,md.value FROM ("

	for index, value := range out {
		if index == 0 {
			query = query + "SELECT \"" + value[1] + "\" as keyMsg"
		} else {
			query = query + " UNION ALL " + " SELECT \"" + value[1] + "\" as keyMsg"
		}
	}
	query = query + ") AS list LEFT JOIN messages m ON list.keyMsg = m.keyMsg LEFT JOIN messageDetails  md on m.id=md.messageId and md.language= ?"
	rows, err := db.Query(query, previewMsgDetail.Language)
	defer rows.Close()
	for rows.Next() {
		parsedmessageDetail := PreviewMessageDetails{}

		if err := rows.Scan(&parsedmessageDetail.KeyMsg, &parsedmessageDetail.Value); err != nil {
			strings.ReplaceAll(value, "<h2>{{"+parsedmessageDetail.KeyMsg+"}}", parsedmessageDetail.KeyMsg+"+Key not found</h2>")
			// return value, err
		}

		if parsedmessageDetail.Value != "" {
			value = strings.ReplaceAll(value, "{{"+parsedmessageDetail.KeyMsg+"}}", parsedmessageDetail.Value)
		} else {
			strings.ReplaceAll(value, "<h2>{{"+parsedmessageDetail.KeyMsg+"}}", parsedmessageDetail.KeyMsg+"+Key not found</h2>")

		}

	}

	if err != nil {
		return value, err
	}
	return value, nil
}

func (store *dbStore) ParseTemplateMessageDetails(previewTemplateMsgDetail *PreviewTemplateMessageDetails) (string, error) {

	var value = strings.TrimSpace(previewTemplateMsgDetail.Value)

	rex := regexp.MustCompile(`\{{(.*?)\}}`)
	out := rex.FindAllStringSubmatch(value, -1)

	if len(out) > 0 {
		query := "SELECT list.keyMsg ,md.value FROM ("

		for index, value := range out {
			if index == 0 {
				query = query + "SELECT \"" + value[1] + "\" as keyMsg"
			} else {
				query = query + " UNION ALL " + " SELECT \"" + value[1] + "\" as keyMsg"
			}
		}
		query = query + ") AS list LEFT JOIN messages m ON list.keyMsg = m.keyMsg LEFT JOIN messageDetails  md on m.id=md.messageId and md.language= ?"
		rows, err := db.Query(query, previewTemplateMsgDetail.Language)
		defer rows.Close()
		for rows.Next() {
			parsedTemplatemessageDetail := PreviewTemplateMessageDetails{}

			if err := rows.Scan(&parsedTemplatemessageDetail.KeyMsg, &parsedTemplatemessageDetail.Value); err != nil {
				strings.ReplaceAll(value, "<h2>{{"+parsedTemplatemessageDetail.KeyMsg+"}}", parsedTemplatemessageDetail.KeyMsg+"+Key not found</h2>")
				// return value, err
			}

			if parsedTemplatemessageDetail.Value != "" {
				value = strings.ReplaceAll(value, "{{"+parsedTemplatemessageDetail.KeyMsg+"}}", parsedTemplatemessageDetail.Value)
			} else {
				value = strings.ReplaceAll(value, "{{"+parsedTemplatemessageDetail.KeyMsg+"}}", "{{"+parsedTemplatemessageDetail.KeyMsg+"}}"+"+Key not found")

			}

		}

		if err != nil {
			return value, err
		}
	}
	return value, nil
}

// The store variable is a package level variable that will be available for
// use throughout our application code
var store Store

/*
We will need to call the InitStore method to initialize the store. This will
typically be done at the beginning of our application (in this case, when the server starts up)
This can also be used to set up the store as a mock, which we will be observing
later on
*/
func InitStore(s Store) {
	store = s
}
