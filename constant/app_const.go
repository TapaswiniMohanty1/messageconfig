package constant

import (
	"bitbucket.org/libertywireless/inventory-management-system/config"
	"github.com/mitchellh/mapstructure"
)

// Environment independent Application constants
const (
	MaxIdleConns           = 5
	MaxOpenConns           = 500
	CountryCode            = "SG"
	DBResultLimit          = 40
	DBInfiniteLimit        = 10000
	CirclesLocationNameMap = "circles"
)

// Secrets

// These secrets are initialized from config.yml
// For every field here also define a getter
type secrets struct {
	KirkAPIKey string `mapstructure:"kirk_api_key"`
	ApiDsn     string `mapstructure:"api_dsn"`
}

var secureConsts = new(secrets)

// InitSecrets initializes all the secure constants
func InitSecrets(sMap map[string]interface{}) {
	mapstructure.Decode(sMap, secureConsts)
}

// Environment dependent variables

// Don't export these variables rather create getters
// else they won't behave as constants
var (
	middlewareEndpoint = "http://10.30.1.14:9080"
	kirkEndpoint       = "http://10.20.90.19:7080"
)

// InitConstants initialises Environment specific constants
func InitConstants(env *config.MiscDataConfig) {
	// updated the constant values
	if env.Production() {
		middlewareEndpoint = "http://10.20.1.51:9080"
		kirkEndpoint = "http://10.20.1.27:7080"
	} else if env.Staging() {

	}
}

// Getters for Environment dependent variables`

// MiddlewareEndpoint => `middlewareEndpoint`
func MiddlewareEndpoint() string {
	return middlewareEndpoint
}

// KirkEndpoint => `kirkEndpoint`
func KirkEndpoint() string {
	return kirkEndpoint
}

// Getters for Secure constants

// KirkAPIKey => `KirkAPIKey`
func KirkAPIKey() string {
	return secureConsts.KirkAPIKey
}

// ApiDsn => `ApiDsn`
func ApiDsn() string {
	return secureConsts.ApiDsn
}
