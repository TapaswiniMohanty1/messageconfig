var arrHead = new Array();
arrHead = [  'TemplateCategory',"templateName",'Template','templateDesc','language','Preview','Action'];

lang = [  'EN', 'CH','IN'];
category = [  'EMAIL', 'SMS','PUSH'];



function addTemplate() {
    
    var appName = $( "#appname-dropdown option:selected" ).text();
    if (appName == "")
    {
    alert("Please select an Application name");
    return false;
    }
    var jiranumber = document.getElementById('jiranumbernew').value;
    if (jiranumber == "")
    {
    alert("Please input a jiranumber");
    return false;
    }
   var empTab = document.getElementById('newMessageTable');
    var rowCnt = empTab.rows.length;        // GET TABLE ROW COUNT.
    var tr = empTab.insertRow(rowCnt);      // TABLE ROW.
    for (var c = 0; c < arrHead.length; c++) {
        var td = document.createElement('td'); 
        td = tr.insertCell(c);
        if (c == 0){
            var s1 = $('<select id="category_dropdown" multiple size="4" />'); 
            for (var i=0;i<category.length;i++)
            {   
             $('<option />', {value: category[i], text: category[i]}).appendTo(s1); 
            } 
            s1.appendTo(td);
            td.width = '15%';
        }  else if (c == 2 || c == 3){
        
            var input = document.createElement('TEXTAREA');
                input.setAttribute('name', 'post');
                //input.setAttribute('maxlength', 5000);
                input.setAttribute('cols',25);
                input.setAttribute('rows', 2);
                input.setAttribute('value','');
                td.appendChild(input);
                td.width = '20%';
            }
            else if ( c == 1 ) {  
            var input = document.createElement('input');
            input.setAttribute('type', 'text');
            input.setAttribute('value', '');
            td.appendChild(input);
            td.width = '15%'
              
        }else if (c==4){
            var s1 = $('<select id="language_prev_dropdown" />'); 
            for (var i=0;i<lang.length;i++)
            {   
             $('<option />', {value: lang[i], text: lang[i]}).appendTo(s1); 
            } 
            s1.appendTo(td);
            td.width = '15%';
        } else if (c==5){
            var button = document.createElement('input');
            button.setAttribute('type', 'button');
            button.setAttribute('value', 'Preview');
            button.setAttribute('onclick', 'preview(this)');
            td.appendChild(button);
            td.width = '15%';
        }  else if(c==6){         
            var input = document.createElement('input');
            input.setAttribute('type', 'button');
            input.setAttribute('value', 'Remove');
            input.setAttribute('onclick', 'removeRow(this)');
            td.appendChild(input);
            td.width = '15%'           
    
        }  
      
    }
  }




 function removeRow(oButton) {
    var empTab = document.getElementById('newMessageTable');
    empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);
}

function preview(oButton) {
    var appName = $( "#appname-dropdown option:selected" ).text();
    if (appName == "")
    {
    alert("Please select an Application name");
    return false;
    }

    var empTab = document.getElementById('newMessageTable');
    var language =  $( "#language_prev_dropdown option:selected" ).text();
    if (language == "")
    {
    alert("Please select a language  for preview.");
    return false;
    }    
    var templateName = oButton.parentNode.parentNode.cells[1].childNodes[0].value;
    var categories = [];
    $.each($("#category_dropdown option:selected"), function(){
        categories.push($(this).val());
    });

    var templateval = oButton.parentNode.parentNode.cells[2].childNodes[0].value;
    var templateDesc = oButton.parentNode.parentNode.cells[3].childNodes[0].value;


    previewMsg={
        "appName" : appName,
        "value" : templateval,
        "language"   : language
    };
    var previewMsgObjStr = JSON.stringify(previewMsg);
    jQuery.ajax({
        type: 'POST',
        url: '/previewTemplatemessage',
        data: previewMsgObjStr,
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            console.log('success');
            var h = 650;
            var w = 340;
            var wh = screen.height;
            var ww = screen.width;
            var top = wh/2 - h/2;
            var left = ww/2 - w/2;
            var popup = window.open( '', 'player', 'height=' + wh + ', width=' + w + ', scrollbars=no, left=' + left + ', top=' + top);
            var $popup = $(popup.document.body);
            $popup.html('<textarea style="height:50%;width:100% ">'+data+'</textarea>');


        },
        error: function (xhr, textStatus, errorThrown) {
            console.log('Error in Operation');
        }
    });
}
function editRow(){
    $('.editbtn').click(function () {
        var currentTD = $(this).closest('tr');

        if ($(this).html() == 'Edit') {
           $(currentTD).find('.inputDisabled').prop("disabled",false);
        } else {
           $(currentTD).find('.inputDisabled').prop("disabled",true);

        }
    });


}


function submitTemplate() {
    var myTab = document.getElementById('newMessageTable');
    var jiranumber = document.getElementById('jiranumbernew').value;
    var appName = $( "#appname-dropdown option:selected" ).text();
    var templateMessageList = [];

    // LOOP THROUGH EACH ROW OF THE TABLE.
    for (row = 1; row < myTab.rows.length ; row++) {
        var name = myTab.rows.item(row).cells[1].childNodes[0].value;
        var value = myTab.rows.item(row).cells[2].childNodes[0].value;
        var description = myTab.rows.item(row).cells[3].childNodes[0].value;

        var categories = [];
        var selectedOptions = myTab.rows.item(row).cells[0].children[0].selectedOptions;
            var selectedValue;   
            for (option = 0; option < selectedOptions.length ; option++) {
                selectedValue = myTab.rows.item(row).cells[0].children[0].selectedOptions[option].text;
                if (typeof selectedValue != 'undefined' && selectedValue != "" && $.inArray(selectedValue, categories) == "-1") {        
                    templateMessageList.push({
                        "appName" : appName,
                        "jiranumber" :  jiranumber,
                        "templateName" : name,
                        "value" : value,
                        "description"  : description,
                        "templateCatagory"   : selectedValue
                    });
                }                     
            }    
  
}
saveTemplateMessages(templateMessageList);
}


function saveTemplateMessages(jsonObj){
    var obj = jsonObj[0];
    var myObjStr = JSON.stringify(obj);
    jQuery.ajax({
        type: 'POST',
        url: '/templateMessage',
        data: myObjStr,
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            addTemplateMessageDetails(jsonObj);  
            document.location.reload() ;
    
          },
        error: function (xhr, textStatus, errorThrown) {
            console.log('Error in Operation');
        }
    });


}
function addTemplateMessageDetails(jsonObj){
    for(var i = 0; i < jsonObj.length; i++) {
        var obj = jsonObj[i];
        var myObjStr = JSON.stringify(obj);
        jQuery.ajax({
          type: 'POST',
          url: '/templateMessageDetails',
          data: myObjStr,
          dataType: 'json',
          success: function (data, textStatus, xhr) {
          },
          error: function (xhr, textStatus, errorThrown) {
              console.log('Error in Operation');
          }
      });

      }
      document.location.reload() ;
}


$(document).ready(function () {

    $("#upload").click(function (event) {
        var appName = $( "#table-appname-dropdown option:selected" ).text();
        if (appName == "")
        {
        alert("Please select an Application name");
        return false;
        }


        //stop submit the form, we will post it manually.
        event.preventDefault();

        // var form = $('#uploadform')[0];
        // var data = new FormData(form);

        var formData =$("#uploadform").submit(function(e){
            return ;
        });
        var formData = new FormData(formData[0]);


                // disabled the submit button
        $("#upload").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/upload",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#upload").prop("disabled", false);
                document.location.reload() ;

            },
            error: function (e) {
			$("#result").text(e.responseText);
			                console.log("ERROR : ", e);
			                $("#upload").prop("disabled", false);

			            }
			        });

			    });

			    let dropdown = $('#appname-dropdown');
			    let dropdowntable = $('#table-appname-dropdown');
			    let exportTab_dropdown  = $('#exportTab-appname-dropdown');


			    dropdowntable.empty();
			    dropdowntable.prop('selectedIndex', 0);
			    const url = '/appNames';

			    dropdown.empty();
			    dropdown.prop('selectedIndex', 0);
			    // Populate dropdown with list of appNames
			    $.getJSON(url, function (data) {
			      $.each(data, function (key, entry) {
			        dropdown.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
			        dropdowntable.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
			        exportTab_dropdown.append($('<option></option>').attr('value', entry.appName).text(entry.appName));
			      })
			    });


			});

			