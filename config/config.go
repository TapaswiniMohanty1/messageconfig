package config

import (
	"bitbucket.org/libertywireless/messageConfig/utils"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

// DbConfig defines structure of persistent database config
type DbConfig struct {
	Dsn string `mapstructure:"dsn"`
}

type LoggingConfig struct {
	LogFilePath   string `mapstructure:"log_file_path"`
	LogLevel      string `mapstructure:"log_level"`
	LogRotation   string `mapstructure:"log_rotation"`
	LogFormatJson bool   `mapstructure:"log_format_json"`
}

// GeneralConfig defines structure of general config
type GeneralConfig struct {
	LoggingConfig *LoggingConfig
	DbConfig      *DbConfig
	Locale        string
}

// LoadConfig loads config from a file path
func LoadConfig(filePath string) (a *GeneralConfig) {

	viper.SetConfigFile(filePath)

	err := viper.ReadInConfig()
	utils.HandleError(err)

	config := new(GeneralConfig)

	config.LoggingConfig = new(LoggingConfig)
	config.DbConfig = new(DbConfig)

	mapstructure.Decode(viper.GetStringMap("logging"), config.LoggingConfig)
	mapstructure.Decode(viper.GetStringMap("db"), config.DbConfig)
	config.Locale = viper.GetString("locale")

	return config
}
